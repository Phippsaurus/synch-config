#!/bin/zsh

function cpp-init-project {
  if (( $# == 0 ))
  then echo "Usage: cpp_init project_name";
  else
  mkdir -p $1/build/debug $1/build/release $1/src $1/include
  cd $1

  printf "cmake_minimum_required(VERSION 3.10.2)

project($1 CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_FLAGS_DEBUG \"\${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra -pedantic \\
-Werror -Wfatal-errors -Wno-error=ignored-qualifiers\")
set(CMAKE_CXX_FLAGS \"\${CMAKE_CXX_FLAGS} -march=native\")

find_package(Threads REQUIRED)

add_executable($1
\tsrc/main.cpp)

target_include_directories($1 PRIVATE
\t\${PROJECT_SOURCE_DIR}/include/)

target_link_libraries($1
\t\${CMAKE_THREAD_LIBS_INIT})
" >> CMakeLists.txt

  printf "#include <iostream>
#include <string>
#include <vector>

int main(int argc, char** argv) {
  std::vector<std::string> args{argv + 1, argv + argc};
  return 0;
}

" >> src/main.cpp

  printf "build/*\n" >> .gitignore

  (cd build/debug &&
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=yes ../..)
  ln -s build/debug/compile_commands.json ./
  git init
  git add .
  git commit -m "Initialize project $1"
  cd ..
  fi
}

function cpp-add-class {
  if (( $# == 0 ))
  then echo "Usage: add_class class_name";
  else
  include_guard=$1:u"_H"
  printf "#if !defined($include_guard)
#define $include_guard

class $1 {

public:

};

#endif

" >> include/$1.h
  printf "#include \"$1.h\"\n\n" >> src/$1.cpp
  sed -i '/add_executable(/a \	src/'"$1"'.cpp' CMakeLists.txt
  git add CMakeLists.txt src/$1.cpp include/$1.h
  fi
}
