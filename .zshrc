if [ "$XDG_CONFIG_HOME" = "" ]; then
	export XDG_CONFIG_HOME=$HOME/.config
fi
if [ "$TMUX" = "" ]; then
	tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf new-session -A -s Hello
fi
# If you come from bash you might have to change your $PATH.
export PATH=$PATH:~/.cargo/bin/:~/go/bin/:~/.local/bin/:$(ruby -e 'print Gem.user_dir')/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/:/usr/local/lib/

# Path to your oh-my-zsh installation.
export ZSH=/home/philipp/.oh-my-zsh

# Adjust java path for android development
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk

# Specify that I belong on this machine (no need for user@host)
DEFAULT_USER=`whoami`

# FZF related options
export DISABLE_FZF_KEY_BINDINGS='true'
export FZF_DEFAULT_OPTS="--preview '~/Utils/fzf-preview.sh {}'"

# use vim as manpage viewer
export MANPAGER="/bin/sh -c \"unset PAGER; \
	col -b -x | \
	vim -l --noplugin -u NONE -R \
	--cmd 'so ~/.vim/pager.vimrc' -\""

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="spaceship"
SPACESHIP_RUST_SYMBOL=""
SPACESHIP_CHAR_SYMBOL=" "
SPACESHIP_JOBS_SYMBOL=" "

# Set vim keybindings before loading plugins becaue otherwise their keybindings do not work
bindkey -v

# For vi mode to work properly
export RPS1="%{$reset_color%}"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	git
	cargo
	fzf
	zsh-syntax-highlighting
	emoji
	)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Aliases
alias ff='firefox-nightly'
alias :e='nvim'
alias ls='exa --icons'
alias ssh-add='ssh-add -t 1h'
alias tmux='tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf'
alias config='/usr/bin/git --git-dir=$HOME/.synch-config/ --work-tree=$HOME'
alias pkglist='yay -Qqe > $HOME/Utils/pkglist.txt'
alias pkgrestore='yay -S --needed $(/usr/bin/cat $HOME/Utils/pkglist.txt)'
alias wiki='nvim -c :VimwikiIndex'
alias ciao='poweroff'

compdef config='git'
compdef ssh-add='ssh-add'

# Settings for vim readline
function zle-line-init zle-keymap-select() {
	case "${KEYMAP}" in
		"vicmd")
			echo -ne '\033[2 q' ;;
		"main"|"viins"|"")
			echo -ne '\033[6 q' ;;
	esac

	zle reset-prompt
	zle -R
}

# Ensure that the prompt is redrawn when the terminal size changes.
TRAPWINCH() {
  zle &&  zle -R
}

zle -N zle-keymap-select
zle -N edit-command-line

# Better searching in command mode
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward

# Make escape key more reactive
KEYTIMEOUT=18 # * 10 ms

# Beginning search with arrow keys
bindkey -M viins "^[OA" up-line-or-beginning-search
bindkey -M viins "^[OB" down-line-or-beginning-search
bindkey -M vicmd "^[OA" up-line-or-beginning-search
bindkey -M vicmd "^[OB" down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search
bindkey kj vi-cmd-mode
bindkey "\e" vi-cmd-mode

function change-cursor-on-accept() {
	echo -ne '\033[2 q'
	zle accept-line
}
zle -N change-cursor-on-accept change-cursor-on-accept
bindkey -M viins "^M" change-cursor-on-accept

function vi_mode_prompt_info() {
  echo "${${KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
}

# define right prompt, if it wasn't defined by a theme
if [[ "$RPS1" == "" && "$RPROMPT" == "" ]]; then
  RPS1='$(vi_mode_prompt_info)'
fi

# emoji
function insert-emoji() {
	LBUFFER+="$(display_emoji | fzf --preview-window=hidden | sed -e 's/\s*=.*$//')"
}
zle -N insert-emoji
bindkey "^[[Z" insert-emoji
# use gpg-agent for ssh
# unset SSH_AGENT_PID
# if [[ "${gnupg_SSH_AUTH_SOCK_by:-0}"  != $$ ]]; then
#     export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
# fi
# export GPG_TTY=$(tty)
# gpg-connect-agent updatestartuptty /bye > /dev/null
eval `ssh-agent -s` > /dev/null

# Custom environment variables
export RUST_SRC_PATH=/home/philipp/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src
export EDITOR='nvim'

# Add optional zsh commands
autoload -Uz zmv
# Add external scripts
source ~/Utils/cpp-project.sh

# OCaml for now
test -r /home/philipp/.opam/opam-init/init.zsh \
	&& ./home/philipp/.opam/opam-init/init.zsh > /dev/null 2> /dev/null \
	|| true
