" tagbar code structure
packadd! tagbar
let g:tagbar_sort=0
let g:tagbar_compact=1

set tabstop=2 softtabstop=2 shiftwidth=2 expandtab smarttab
