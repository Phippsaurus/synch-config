" No compatibility – necessary for mappings to work.
set nocompatible

set mouse=a
set termguicolors

" Turn syntax on
syntax enable
set ft=man nomod nolist

packadd! fsharp-vim
packadd! vim-toml
packadd! rust.vim
packadd! gruvbox
packadd! matchit
packadd! AnsiEsc.vim

set incsearch
set hlsearch
colorscheme gruvbox
" Status line
set ruler
set laststatus=0
set cmdheight=1
set nomodifiable " Only in version 6.0
set readonly

" My xterms have a navy-blue background, so I need this line too.
set background=dark

nnoremap <silent> q :q!<CR>
