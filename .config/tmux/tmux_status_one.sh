#!/bin/sh
read -r state percent <<<$(acpi -b | awk '{gsub(",",""); print $3,$4}')

if [[ "$state" = "Discharging" ]]; then
	case $percent in
		100%) bat_sym=" "; bat_color="#98c379";;
		9[0-9]%) bat_sym=" "; bat_color="#98c379";;
		[7-8][0-9]%) bat_sym=" "; bat_color="#98c379";;
		[5-6][0-9]%) bat_sym=" "; bat_color="#98c379";;
		[3-4][0-9]%) bat_sym=" "; bat_color="#e5c07b";;
		[1-2][0-9]%) bat_sym=" "; bat_color="#d19a66";;
		[0-9]%) bat_sym=""; bat_color="#be5046";;
	esac
else
	bat_sym=" "; bat_color="#98c379"
fi

dev=$(iw dev | awk '/Interface/ {print $2}')
wlan=$(iw dev "$dev" link)
if [[ "$wlan" = "Not connected." ]]; then
	wlan_color="#be5046"; wlan_str="睊"
else
	ssid=$(echo "$wlan" | awk '/SSID/ {print $2}')
	speed=$(echo "$wlan" | awk '/tx bitrate/ {print $3,$4}')
	wlan_color="#61afef"; wlan_str="直$ssid $speed "
fi

printf "#[fg=$wlan_color,bg=#504945,nobold,nounderscore,noitalics]\
	#[fg=#504945,bg=$wlan_color]$wlan_str\
	#[fg=$bat_color,nobold,nounderscore,noitalics,bg=$wlan_color]\
	#[fg=#504945,bold,bg=$bat_color]$bat_sym%4s " $percent
