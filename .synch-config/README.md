# Configuration Synchronization for Arch-based System

The setup for the `.synch-config` repository is taken from
[here](https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/).

## Initialize Synchronization on Fresh System

Custom steps to initialize the configuration synching are:

```sh
git clone --bare git@bitbucket.org:Phippsaurus/synch-config.git \
$HOME/.synch-config
```

Create an alias for `config`:

```sh
alias config='/usr/bin/git --git-dir=$HOME/.synch-config --work-tree=$HOME'
```

Then backup with:

```sh
mkdir -p $HOME/.config-backup && config checkout 2>&1 | egrep "\s+\." | \
awk {'print $1'} | xargs -I{} mv {} $HOME/.config-backup/{}
```

And finally set the local repository up to ignore untracked files, which would
contain the entire home directory otherwise.

## Plugins

To also clone plugins (e.g. for vim) referenced as submodules use:

```sh
config submodule update --init
```

## Package List Backup

The alias `pkglist` writes the list of packages currently installed from the
pacman repositories and the _AUR_ to `$HOME/Utils/pkglist.txt`.

This list is part of the synchronization repository.

To restore the package list on a fresh install, after installing `aurman`, use
the alias `pkgrestore` (new `zsh` session required, or `source $HOME/.zshrc` to
register the alias).
